using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
  public float speed = 1.0f;
  public AudioSource[] shootSounds;
  public Vector3 destination;
  private int shootSoundVariations = 5;

  void Start()
  {
    shootSounds[Random.Range(0, shootSoundVariations - 1)].GetComponent<AudioSource>().Play();
  }

  void Update()
  {
    Move();
  }

  void Move()
  {
    float step = speed * Time.deltaTime;
    transform.position = Vector3.MoveTowards(transform.position, destination, step);
  }
}
