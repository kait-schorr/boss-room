using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class FireballSpawner : MonoBehaviour
{
  public GameObject dragonHead;
  public GameObject fireball;
  public float timeBetweenShots = 1;
  private float originalTimeBetweenShots;

  void Start()
  {
    originalTimeBetweenShots = timeBetweenShots;
  }

  void Update()
  {
    if (timeBetweenShots > 0)
    {
      timeBetweenShots -= Time.deltaTime;
    } else
    {
      if (Input.GetMouseButtonDown(0)) ShootFireball();
    }
  }

  void ShootFireball()
  {
    Vector3 mouseClickFromCamera = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    mouseClickFromCamera.z = 0;

    GameObject spawnedFireball = Instantiate(fireball, dragonHead.transform.position, Quaternion.identity);
    spawnedFireball.GetComponent<Fireball>().destination = mouseClickFromCamera;

    timeBetweenShots = originalTimeBetweenShots;
  }
}
