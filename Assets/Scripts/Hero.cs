using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour
{
  public float speed = 6.0f;
  public float timeToMove = 3;
  public float yLane = 0.0f;
  private float originalTimeToMove;
  private float[] xPositions = new float[]{0.0f, 1.0f, 2.0f, 3.0f, 4.0f};
  private int numberOfPositions = 5;
  private int currentPosition = 0;

  void Start()
  {
    originalTimeToMove = timeToMove;
  }

  void Update()
  {
    if (timeToMove > 0)
    {
      timeToMove -= Time.deltaTime;
    } else
    {
      timeToMove = originalTimeToMove;
      if (currentPosition + 1 < numberOfPositions) currentPosition += 1;
    }
    MoveTowardsDragon();
  }

  void MoveTowardsDragon()
  {
    float step = speed * Time.deltaTime;
    Vector2 targetPosition = new Vector2(xPositions[currentPosition], yLane);
    transform.position = Vector2.MoveTowards(transform.position, targetPosition, step);
  }
}
