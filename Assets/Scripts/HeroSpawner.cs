using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroSpawner : MonoBehaviour
{
  public GameObject hero;
  public float timeRemaining = 3;
  public int numberOfHeros = 2;
  private float originalTimeRemaining;
  private float[] yPositions = new float[]{4.0f, 2.0f, 0.0f, -2.0f, -4.0f};
  private int numberOfYPositions = 5;

  void Start()
  {
    originalTimeRemaining = timeRemaining;
  }

  void Update()
  {
    if (timeRemaining > 0)
    {
      timeRemaining -= Time.deltaTime;
    } else
    {
      bool[] spawnedPositions = new bool[]{false, false, false, false, false};
      for(int index = 0; index < numberOfHeros; index++)
      {
        int randomY;
        do
        {
          randomY = Random.Range(0, numberOfYPositions);
        } while(spawnedPositions[randomY]);
        spawnedPositions[randomY] = true;

        SpawnHero(yPositions[randomY]);
      }
    }
  }

  void SpawnHero(float heroYLane)
  {
    float heroStartingX = -3.0f;
    float heroStartingY = 0.0f;
    Vector2 heroStartingPosition = new Vector2(heroStartingX, heroStartingY);
    timeRemaining = originalTimeRemaining;
    GameObject spawnedHero = Instantiate(hero, heroStartingPosition, Quaternion.identity);
    spawnedHero.GetComponent<Hero>().yLane = heroYLane;
  }
}
